import React, { Component } from 'react';
import ProductsContainer from './ProductsContainer';
import CartContainer from './CartContainer';
import FilterContainer from './FilterContainer';
//import CategoriesContainer from './CategoriesContainer';
import './App.scss';
import logo from '../img/logo-2.png'
import banner from '../img/banner.jpg'
import fondoCompras from '../img/flowery-garden.jpg'
import {
  Navbar, NavbarBrand, Nav, NavItem, NavLink, Modal, ModalBody, ModalFooter, Button, ModalHeader,
  FormFeedback, Label, FormGroup, Input
} from 'reactstrap'
import Registrar from "../components/Register";
import Swal from 'sweetalert2'
const axios = require('axios');

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      showRegister: false,
      username: '',
      password: '',
      validate: {
        username: false,
        password: false
      },

      redirectRegistar: false,
      estadoConexion: false,

      estadoSesion: "Ingresar"
    };

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openRegister = this.openRegister.bind(this);
  }
  componentWillMount() {

    const x = document.cookie;
    const ArrayX = x.split("=");
    if (x !== "") {
      if (ArrayX[1] !== "") {
        this.setState({
          estadoSesion:"Cerrar Sesión",
          estadoConexion:true
        })
      }else{
        this.setState({
          estadoSesion:"Ingresar"
        })
      }
    }
  }
    handleChange = event => {
      let change = {};
      change[event.target.name] = event.target.value;
      this.setState(change)
    };

    handleSubmit() {
      const self = this;
      if (this.state.username === "") {
        Swal.fire({
          title: 'Error!',
          text: 'Colocar email',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      } else if (this.state.password === "") {
        Swal.fire({
          title: 'Error!',
          text: 'Colocar contraseña',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      } else {
        axios.post('https://kunanpaback.herokuapp.com/loginUsuario', {
          email: this.state.username,
          password: this.state.password,
        })
          .then(function (response) {
            if (response.status === 200) {
              if (response.data.codigo === "1") {
                Swal.fire({
                  title: 'Ingreso Correcto!',
                  type: 'success',
                  confirmButtonText: 'Ok'
                })
                document.cookie = "username=" + self.state.username;
                self.setState({
                  show: false,
                  estadoConexion: true,
                  estadoSesion: "Cerrar Sesión"
                })
              } else if (response.data.codigo === "2") {
                Swal.fire({
                  title: 'Error!',
                  text: 'Correo no registrado',
                  type: 'error',
                  confirmButtonText: 'Ok'
                })
              } else if (response.data.codigo === "3") {
                Swal.fire({
                  title: 'Error!',
                  text: 'Constraseña incorrecta',
                  type: 'error',
                  confirmButtonText: 'Ok'
                })
              } else {

              }
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
    handleClose() {
      this.setState({ show: false });
    }

    handleShow() {
      if (this.state.estadoConexion) {
        document.cookie = "username=";
        this.setState({
          estadoConexion: false,
          estadoSesion: "Ingresar"
        });

      } else {
        this.setState({ show: true });
      }
    }
    openRegister() {
      this.setState({ redirectRegistar: true })
    }

    render() {
      if (this.state.redirectRegistar) {
        return (
          <Registrar />
        );
      }
      return (
        <div>
          <div className="main-container-banner" style={{ backgroundImage: "url(" + banner + ")" }}>
            <Navbar expand="md">
              <NavbarBrand href="/">
                <div style={{ marginLeft: '65px', marginRight: '65px', }}>
                  <img src={logo} alt="Logo" style={{ width: '95px', marginTop: '25px' }} />
                </div>
              </NavbarBrand>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink>
                    <button onClick={this.handleShow} style={{ marginLeft: '10px', marginRight: '15px' }} type="button" className="btn btn-outline-warning">
                      <span style={{ padding: '2px', marginLeft: '10px', marginRight: '15px', fontSize: '20px', color: 'white', cursor: 'pointer' }} >{this.state.estadoSesion}</span>
                    </button>
                  </NavLink>
                </NavItem>
              </Nav>
            </Navbar>
            <br />
            <div className="banner__main">
              <h1 className="banner__title">
                <b>Una ofrenda a la memoria de tus seres queridos</b>
              </h1>
              <p className="banner__description">
                Llevamos ofrendas a tus seres queridos, en el momento que más quieras y completamente online
            </p>
              <button className="banner__btn" >
                <a href="#comprar" style={{ color: 'white' }}>Arma tu arreglo</a>
              </button>
            </div>
          </div>
          <div className="main-container">
            <br />
            <h2 className="section-video__title">
              Veamos como funciona
          </h2>
            <div className="section-video__content">
              <div className="video" style={{ fontSize: '1em' }}>
                <div className="video__content">
                  <div className="video__left">
                    <h3 className="video__title">Kunanpa</h3>
                    <p className="video__description">
                      Los arreglos de florales son una muestra de cariño para homenajear a tus seres queridos.<br />
                      Nuestra labor consiste en colocar las flores en el lugar que tu desees.
                  </p>
                  </div>
                  <div className="video__right">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/5NSG58gcwus?rel=0" title="presentacion" frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen="allowfullscreen">
                    </iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ backgroundImage: "url(" + fondoCompras + ")", boxShadow: 'inset 0 0 0 1000px rgba(0, 0, 0, 0.6)', backgroundSize: 'contain', }} id="comprar">
            <div className="main-container"  >
              <div className="filter-container">
                <FilterContainer />
                <div className="cart-container">
                  <CartContainer />
                </div>
              </div>
              {/*<div className="categories-container">
              <CategoriesContainer />
    </div>*/}
              <div className="products-container">
                <ProductsContainer />
              </div>
            </div>
          </div>
          <Modal id="loginModal" isOpen={this.state.show} hidden={this.handleClose}>
            <ModalHeader>
              Iniciar Sesión
          </ModalHeader>
            <ModalBody>
              <FormGroup style={{ marginRight: '50px', marginLeft: '50px' }}>
                <div className="text-center">
                  <Input
                    name="username"
                    id="usuarioInput"
                    placeholder="Nombre de Usuario"
                    value={this.state.username}
                    valid={this.state.validate.username === "has-success"}
                    invalid={this.state.validate.username === "has-danger"}
                    onChange={this.handleChange}
                    autoFocus
                    onKeyPress={this.handleKeyPress}
                  />
                  <FormFeedback invalid>Usuario Incorrecto</FormFeedback>
                </div>
              </FormGroup>
              <FormGroup style={{ marginRight: '50px', marginLeft: '50px' }}>
                <div className="text-center">
                  <Input
                    name="password"
                    id="usuarioInput"
                    placeholder="Contraseña"
                    type="password"
                    value={this.state.password}
                    valid={this.state.validate.password === "has-success"}
                    invalid={this.state.validate.password === "has-danger"}
                    onChange={this.handleChange}
                    autoFocus
                    onKeyPress={this.handleKeyPress}
                  />
                  <FormFeedback invalid>Contraseña Incorrecto</FormFeedback>
                </div>
              </FormGroup>
              <div className="text-right" style={{ marginRight: '50px', marginLeft: '50px', marginTop: '15px' }}>
                <Label style={{ cursor: 'pointer' }} onClick={this.openRegister}>Registrarme</Label>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button variant="secondary" onClick={this.handleClose}>
                Cancelar
            </Button>
              <Button variant="btn btn-success" onClick={this.handleSubmit}>
                Iniciar Sesión
            </Button>
            </ModalFooter>
          </Modal>
        </div>

      )
    }
  };

  export default App;
