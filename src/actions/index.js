import shop from '../api/shop';
import * as types from '../constants/ActionTypes';
import "bootstrap/dist/css/bootstrap.css";
import Swal from 'sweetalert2'
const axios = require('axios')


const receiveProducts = products => ({
  type: types.RECEIVE_PRODUCTS,
  products,
});

const receiveCategories = categories => ({
  type: types.RECEIVE_CATEGORIES,
  categories,
});

const maxPrice = products => ({
  type: types.MAX_PRODUCT_PRICE,
  products,
});

const applyFilter = filter => ({
  type: types.FILTER_PRODUCTS,
  format: filter.format,
  filter: filter.selection,
  text: filter.text,
});

export const getAllProducts = () => dispatch => {
  axios.get('https://kunanpaback.herokuapp.com/productos')
  .then(function (response) {
    dispatch(receiveProducts(response.data.products));
  })
  .catch(function (error) {
    console.log("Error:"+error)
  })
};

export const getAllCategories = () => dispatch => {
  axios.get('https://kunanpaback.herokuapp.com/categorias')
  .then(function (response) {
    dispatch(receiveCategories(response.data.categories));
  })
  .catch(function (error) {
    console.log("Error:"+error)
  })
  /*shop.getCategories(categories => {
    dispatch(receiveCategories(categories));
  });*/

};

export const getMaxPrice = () => dispatch => {
  axios.get('https://kunanpaback.herokuapp.com/productos')
  .then(function (response) {
    dispatch(dispatch(maxPrice(response.data.products)));
  })
  .catch(function (error) {
    console.log("Error:"+error)
  })
};

const addToCartUnsafe = productId => ({
  type: types.ADD_TO_CART,
  productId,
});

const removeItemFromCart = productId => ({
  type: types.REMOVE_FROM_CART,
  productId
})

const updateProductQuantity = cartData => ({
  type: types.UPDATE_PRODUCT_FROM_CART,
  productId: cartData.productId,
  quantity: cartData.quantityProduct,
})

export const addToCart = productId => (dispatch, getState) => {
  if (getState().products.byId[productId].quantity > 0) {
    dispatch(addToCartUnsafe(productId));
  }
};

export const doFilter = (selection,format,text = null) => dispatch => {
  dispatch(applyFilter({selection,format, text}))
}

export const removeFromCart = productId => (dispatch, getState) => {
  const quantityProduct = getState().cart.quantityById[productId];
  dispatch(removeItemFromCart(productId));
  dispatch(updateProductQuantity({productId, quantityProduct}));

}

export const checkout = products => (dispatch, getState) => {
  const { cart } = getState();
  const x = document.cookie;
  const ArrayX = x.split("=");
  if(x !== ""){
    
    if(ArrayX[1] !== ""){
      axios.get('https://kunanpaback.herokuapp.com/productos',{
      correo:ArrayX[1],
      mensaje:"Usted ha realizado una compra, espere uno de nuestros asesores los atienda"
      })
      .then(function (response) {
        Swal.fire({
          title: 'Comprado',
          text: 'Ha realizado la compra',
          type: 'success',
          confirmButtonText: 'Ok'
        })
      })
      .catch(function (error) {
        console.log("Error:"+error)
      })
      dispatch({
        type: types.CHECKOUT_REQUEST,
      });
      shop.buyProducts(products, () => {
        dispatch({
          type: types.CHECKOUT_SUCCESS,
          cart,
        });
      });
    }else{
      Swal.fire({
        title: 'Debe ingresar',
        text: 'Debe ingresar al sistema',
        type: 'error',
        confirmButtonText: 'Ok'
      })
    }
  }else{
    Swal.fire({
      title: 'Debe ingresar',
      text: 'Debe ingresar al sistema',
      type: 'error',
      confirmButtonText: 'Ok'
    })
  }
  
};
