import React from 'react';
import {
    Button,
    FormFeedback, Label, FormGroup, Input, Row, Col, Form, Navbar, NavbarBrand,
} from 'reactstrap'
import App from '../containers/App'
import Swal from 'sweetalert2'

import logo from '../img/logo-2.png'
import banner from '../img/banner.jpg'


const axios = require('axios');

class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            showRegister: false,
            username: '',
            password: '',
            email: '',

            redirectApp: false

        };
        this.regresar = this.regresar.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    validateForm = () => {
        return this.state.username.length > 0 && this.state.password.length > 0;
    };
    regresar() {
        this.setState({
            redirectApp: true
        })
    }
    handleChange = event => {
        let change = {};
        change[event.target.name] = event.target.value;
        this.setState(change)
    };

    validar() {
        if (this.state.username === "") {
            Swal.fire({
                title: 'Error!',
                text: 'Ingrese un nombre',
                type: 'error',
                confirmButtonText: 'Ok'
            })
            return false;
        } else if (this.state.email === "") {
            Swal.fire({
                title: 'Error!',
                text: 'Ingrese un email',
                type: 'error',
                confirmButtonText: 'Ok'
            })
            return false;
        } else if (this.state.password === "") {
            Swal.fire({
                title: 'Error!',
                text: 'Ingrese una contraseña',
                type: 'error',
                confirmButtonText: 'Ok'
            })
            return false;
        } else {
            return true;
        }
    }

    handleSubmit() {
        const self = this;
        if (this.validar()) {
            axios.post('https://kunanpaback.herokuapp.com/newUsuario', {
                email: this.state.email,
                password: this.state.password,
                name: this.state.username
            })
                .then(function (response) {
                    if (response.status === 200) {
                        console.log(response.data.codigo)
                        if (response.data.codigo === "1") {
                            Swal.fire({
                                title: 'Registro Correcto!',
                                type: 'success',
                                confirmButtonText: 'Ok'
                            })
                            self.setState({
                                redirectApp: true
                            })
                        } else {
                            Swal.fire({
                                title: 'Error!',
                                text: 'Correo ya registrado',
                                type: 'error',
                                confirmButtonText: 'Ok'
                            })
                        }
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
    render() {
        const login = {
            backgroundColor: "#f1f1f1",
            borderRadius: "30px",
            marginTop: "80px"
        };
        const buttonSize = {
            boxShadow: '0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19)',
            borderRadius: '8px'
        };
        if (this.state.redirectApp) {
            return (
                <App />
            );
        }
        return (
            <div className="container" >
                <header className="" style={{ backgroundImage: "url(" + banner + ")" }}>
                </header>
                <div>
                    <div >
                        <div className="container" style={login}>
                            <div >
                                <Navbar expand="md">
                                    <NavbarBrand href="/">
                                        <div style={{ marginLeft: '65px', marginRight: '65px', }}>
                                            <img src={logo} alt="Logo" style={{ width: '95px', marginTop: '25px' }} />
                                        </div>
                                    </NavbarBrand>
                                </Navbar>

                                <Row form>
                                    <Col md={4}>
                                    </Col>
                                    <Col md={4}>
                                        <Form>
                                            <div className="text-left" style={{ marginLeft: '120px' }}>
                                                <h3 >Crear cuenta</h3>
                                            </div>
                                            <FormGroup>
                                                <div className="text-left">
                                                    <Label>Usuario</Label>
                                                    <Input
                                                        name="username"
                                                        id="usuarioInput"
                                                        placeholder="Nombre de Usuario"
                                                        type="text"
                                                        value={this.state.username}
                                                        onChange={this.handleChange}
                                                        autoFocus
                                                        onKeyPress={this.handleKeyPress}
                                                    />
                                                </div>
                                            </FormGroup>
                                            <FormGroup>
                                                <div className="text-left">
                                                    <Label>Correo Electrónico</Label>
                                                    <Input
                                                        name="email"
                                                        id="emailInput"
                                                        placeholder="abc@abc.com"
                                                        type="email"
                                                        value={this.state.email}
                                                        onChange={this.handleChange}
                                                        onKeyPress={this.handleKeyPress}
                                                    />
                                                </div>
                                            </FormGroup>

                                            <FormGroup>
                                                <div className="text-left">
                                                    <Label>Contraseña</Label>
                                                    <Input
                                                        name="password"
                                                        id="contraseñaInput"
                                                        placeholder="***********"
                                                        type="password"
                                                        value={this.state.password}
                                                        onChange={this.handleChange}
                                                        onKeyPress={this.handleKeyPress}
                                                    />
                                                    <FormFeedback invalid>Contraseña incorrecta</FormFeedback>
                                                </div>
                                            </FormGroup>
                                            <br />
                                            <br />
                                            <Button
                                                block
                                                size="lg"
                                                disabled={!this.validateForm()}
                                                onClick={this.handleSubmit}
                                                style={buttonSize}
                                                color="primary">
                                                Registrar
                                </Button>
                                            <br />
                                            <Button
                                                block
                                                size="sm"
                                                onClick={this.regresar}
                                                style={buttonSize}
                                                color="danger" >
                                                Regresar
                                </Button>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </Form>
                                    </Col>
                                    <Col md={4}>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
export default Register;
