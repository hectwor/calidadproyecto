import React from 'react';
import PropTypes from 'prop-types';
import './Product.scss';

const Product = ({ price, quantity, name, img }) => (
  <div className="product-text">
    {
      img === "" || img === undefined ? (
        <div>
          <p>{name}</p>
          <span> s/. {price.substr(1)} </span> <br /> <br />
          <span>{quantity ? `Cantidad ${quantity}` : null}</span>
        </div>
      ) : (
          <div>
            <p>{name}</p>
            <img style={{ width: '100%', height: '300px' }} src={img} alt="demo"></img>
            <span> s/. {price.substr(1)} </span> <br /> <br />
            <span>{quantity ? `Stock ${quantity}` : null}</span>
          </div>
        )
    }
  </div>
);

Product.propTypes = {
  price: PropTypes.string,
  quantity: PropTypes.number,
  name: PropTypes.string,
  img: PropTypes.string,
};

export default Product;
