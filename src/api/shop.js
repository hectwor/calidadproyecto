
/**
 * Mocking client-server processing
 */
import _products from './products.json'
//import _categories from './categories.json'

const axios = require('axios');
const TIMEOUT = 100

export default {
  getProducts: (cb, timeout) => setTimeout(() => cb(_products.products), timeout || TIMEOUT),
  //getCategories: (cb, timeout) => setTimeout(() => cb(_categories.categories), timeout || TIMEOUT),
  buyProducts: (payload, cb, timeout) => setTimeout(() => cb(), timeout || TIMEOUT),
  getDistritos: () => {
    axios.get('https://kunanpaback.herokuapp.com/cementerios')
      .then(function (response) {
        let re = "<option disabled value='-1'>Seleccionar</option>";
        response.data.data.forEach(e => {
          re += `<option value="${e.idCementerio}">${e.nombre}</option>`
        });
        document.getElementById("selectCementerios").innerHTML = re;
      })
  },
  getCementerios: (idC) => {
    axios.get('https://kunanpaback.herokuapp.com/idCementerio='+idC)
      .then(function (response) {
        let re = "<option selected disabled value='-1'>Seleccionar</option>";
        response.data.data.forEach(e => {
          re += `<option value="${e.idCementerio}">${e.nombre}</option>`
        });
        document.getElementById("selectCementerios").innerHTML = re;
      })
  }
}